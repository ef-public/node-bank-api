# Node-Bank-API

node.js Express webserver for a basic API


## Build & Run container (API uses port 5000)
```bash
cd node-bank-api
docker image build -t node-bank-api:1.1 .
docker container run -d --rm -p 5000:5000 node-bank-api:1.1
```

## REST API

REST APIs are used to access and manipulate data using a common set of stateless operations. These operations are integral to the HTTP protocol and represent essential create, read, update, and delete (CRUD) functionality, although not in a clean one-to-one manner:
* POST (create a resource or generally provide data)
* GET (retrieve an index of resources or an individual resource)
* PUT (create or replace a resource)
* PATCH (update/modify a resource)
* DELETE (remove a resource)


## API Details

| Route | Description |
| --- | --- |
| GET /api/     | Get server info |
| POST /api/accounts/ | Create an account, e.g.: {"user":"julibcn","currency":"EUR","description":"JuliBCN Account","balance":"100"} |
| GET /api/accounts/:user | Get all data for the specified account |
| DELETE /api/accounts/:user | Remove specified account |
| POST /api/accounts/:user/transactions | Add a transaction, e.g.: {"date":"2022-01-23T18:25:43.511Z","object":"Had a pint","amount":"-5"} |
| DELETE /api/accounts/:user/transactions/:id | Remove specified transaction |


## Sample commands
We can try some basic cURL commands to play with the Bank API:

```bash
curl -X GET http://localhost:5000/api/
curl -X GET http://localhost:5000/api/ -w "\n"
curl -X GET http://localhost:5000/api/accounts/test | jq
```

```bash
curl -H "Content-Type: application/json" -X POST -d '{"user":"julibcn","currency":"EUR","description":"JuliBCN Account","balance":"100"}'  http://localhost:5000/api/accounts/ -w "\n"
curl -X GET http://localhost:5000/api/accounts/julibcn -w "\n" | jq
```

```bash
curl -H "Content-Type: application/json" -X POST -d '{"date":"2022-01-23T18:25:43.511Z","object":"Had a pint","amount":"-5"}' http://localhost:5000/api/accounts/julibcn/transactions -w "\n"
curl -H "Content-Type: application/json" -X POST -d '{"date":"2022-01-23T19:25:43.511Z","object":"Had 2 pints","amount":"-10"}' http://localhost:5000/api/accounts/julibcn/transactions -w "\n"
curl -X GET http://localhost:5000/api/accounts/julibcn | jq
```

```bash
curl -H "Content-Type: application/json" -X POST -d '{"user":"julibcn2","currency":"EUR","description":"JuliBCN2 Account","balance":"500"}'  http://localhost:5000/api/accounts/ -w "\n"
curl -X GET http://localhost:5000/api/accounts/julibcn2 | jq
curl -H "Content-Type: application/json" -X POST -d '{"date":"2022-01-22T18:25:43.511Z","object":"New Samsung Telly","amount":"-399"}' http://localhost:5000/api/accounts/julibcn2/transactions -w "\n"
curl -H "Content-Type: application/json" -X POST -d '{"date":"2022-01-22T10:00:43.511Z","object":"Car Petrol","amount":"-20"}' http://localhost:5000/api/accounts/julibcn2/transactions -w "\n"
curl -X GET http://localhost:5000/api/accounts/julibcn2 | jq
```

```bash
curl -X DELETE http://localhost:5000/api/accounts/julibcn2/transactions/dd9a275f5936bcfac12ccc98f67b236a
curl -X GET http://localhost:5000/api/accounts/julibcn2 | jq
curl -X DELETE http://localhost:5000/api/accounts/julibcn2
curl -X GET http://localhost:5000/api/accounts/julibcn2 | jq
```


## NGinx Plus Reverse Proxy
If we want to put a Reverse Proxy before our API, we can configure the following elements for NGinx. First, add line `return 301 https://$host$request_uri/;` on `default.conf` to forward port 80 to 443:
```bash
nano /etc/nginx/conf.d/default.conf
```

Then add the file `ssl.conf` with some specific configuration:
```bash
nano /etc/nginx/conf.d/ssl.conf
```

```yaml
js_include conf.d/oauth2.js;

# Declaring the bypass of the proxied API
upstream backend {
  server 10.200.0.5;
  }

server {
  listen 443 http2 ssl;
  listen [::]:443 http2 ssl;

#  server_name

  ssl_certificate /etc/ssl/certs/nginx-selfsigned.crt;
  ssl_certificate_key /etc/ssl/private/nginx-selfsigned.key;
  ssl_dhparam /etc/ssl/certs/dhparam.pem;

########################################################################
# from https://cipherli.st/ #
# and https://raymii.org/s/tutorials/Strong_SSL_Security_On_nginx.html #
########################################################################

  ssl_protocols TLSv1.2 TLSv1.3;
  ssl_prefer_server_ciphers on;
  ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
  ssl_ecdh_curve secp384r1;
  ssl_session_cache shared:SSL:10m;
  ssl_session_tickets off;
  ssl_stapling on;
  ssl_stapling_verify on;

  resolver 1.1.1.1 8.8.8.8 valid=300s;
  resolver_timeout 5s;

# Disable preloading HSTS for now. You can use the commented out header line that includes
# the "preload" directive if you understand the implications.
#add_header Strict-Transport-Security "max-age=63072000; includeSubdomains; preload";

  add_header Strict-Transport-Security "max-age=63072000; includeSubdomains";
  add_header X-Frame-Options DENY;
  add_header X-Content-Type-Options nosniff;

##################################
# END https://cipherli.st/ BLOCK #
##################################

  location / {
    root /usr/share/nginx/html;
    index index.html index.htm;
  }

  location /api {
    proxy_pass http://backend/api;
  }

  location /api/secure {
    auth_request /auth_request_token_introspection;
    proxy_pass http://backend/api;
  }

  location = /auth_request_token_introspection {
    internal;
    js_content introspectAccessToken;
  }

  location /_oauth2_send_request {
    internal;
    proxy_method POST;
    proxy_set_header Content-type "application/x-www-form-urlencoded";
#    proxy_set_body "grant_type=client_credentials&client_id=test-app&scope=openid&client_secret=xxxXXXxxxXXXXxxxXXXxxXXXxxxXXxXXXXXXxx1234567890&redire$
    proxy_pass http://backend/api;
 }

  error_page 404 /404.html;
    location = /404.html {
  }

  error_page 500 502 503 504 /50x.html;
    location = /50x.html {
  }
}
```

We will also need to install and load the module for JS by adding the line `load_module modules/ngx_http_js_module.so` inside the statement:
```bash
yum install -y nginx-plus-module-njs
setsebool httpd_can_network_connect 1
nano /etc/nginx/nginx.conf
```

Also, we need the OAuth, in case it is used, to be present:
```bash
nano /etc/nginx/conf.d/oauth2.js
```

```js
function introspectAccessToken(r) {
        r.subrequest("/_oauth2_send_request",
                function(reply) {
                        if (reply.status == 200) {
                                var response = JSON.parse(reply.responseBody);
                                if (response.active == true) {
                                        r.return(204); // Token is valid, return success code
                                } else {
                                        r.return(403); // Token is invalid, return forbidden code
                                }
                        } else {
                                r.return(401); // Unexpected response, return 'auth required'
                        }
                }
          );
      }
```

Now, it is time to test configuration and restart the service:
```bash
sudo nginx -t
systemctl restart nginx.service
```

We will be able to test with the command:
```bash
curl -kX GET https://PUBLIC_IP/api/ -w "\n"
```

However, we will need authentication for this one:
```bash
curl -kX GET https://PUBLIC_IP/api/secure -w "\n"
```
